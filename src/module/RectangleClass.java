package module;

public class RectangleClass {
  private float length = 1.0f;
  private float width = 1.0f;
  
  public RectangleClass() {
  }

  public RectangleClass(float length, float width) {
    this.length = length;
    this.width = width;
  }

  public float getLength() {
    return length;
  }

  public void setLength(float length) {
    this.length = length;
  }

  public float getWidth() {
    return width;
  }

  public void setWidth(float width) {
    this.width = width;
  }

  public double getArea(){
    return this.length*this.width;
  }

  public double getPerimeter(){
    return 2*(this.length + this.width);
  }

  public String toString(){
    return "RectangleClass[length=" + this.length + ", width=" + this.width +"]";
  }
}
