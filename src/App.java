import module.RectangleClass;

public class App {
    public static void main(String[] args) throws Exception {
        RectangleClass rec1 = new RectangleClass();

        RectangleClass rec2 = new RectangleClass(4,5);

        System.out.println("Rectange 1");
        System.out.println(rec1.getArea());
        System.out.println(rec1.getPerimeter());
        System.out.println(rec1.toString());

        System.out.println("Rectange 2");
        System.out.println(rec2.getArea());
        System.out.println(rec2.getPerimeter());
        System.out.println(rec2.toString());
    }
}
